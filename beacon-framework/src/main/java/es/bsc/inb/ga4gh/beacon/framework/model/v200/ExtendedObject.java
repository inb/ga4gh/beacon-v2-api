package es.bsc.inb.ga4gh.beacon.framework.model.v200;

import jakarta.json.JsonValue;
import jakarta.json.bind.annotation.JsonbTransient;
import java.util.Map;

/**
 * This class is to collect all properties from parsing object.
 * 
 * @author Dmitry Repchevsky
 */

public class ExtendedObject {

    private Map<String, JsonValue> additionalProperties;

    @JsonbTransient
    public Map<String, JsonValue> getAdditionalProperties() {
        return additionalProperties;
    }
    
    @JsonbTransient
    public void setAdditionalProperties(Map<String, JsonValue> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}
