package es.bsc.inb.ga4gh.beacon.framework.model.v200;

import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonValue;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.serializer.DeserializationContext;
import jakarta.json.bind.serializer.JsonbDeserializer;
import jakarta.json.stream.JsonParser;
import java.lang.reflect.Type;
import java.util.Collections;

/**
 * @author Dmitry Repchevsky
 */

public class AdditionalPropertiesDeserializer 
        implements JsonbDeserializer<ExtendedObject> {

    private static final Jsonb jsonb = JsonbBuilder.create();

    @Override
    public ExtendedObject deserialize(JsonParser parser, 
            DeserializationContext ctx, Type type) {

        final JsonValue value = parser.getValue();
        if (JsonValue.ValueType.OBJECT == value.getValueType()) {
            final ExtendedObject obj = jsonb.fromJson(value.toString(), type);
            obj.setAdditionalProperties(value.asJsonObject());
            return obj;
        }
        return null;
    }
    
}
