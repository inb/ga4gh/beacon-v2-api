package es.bsc.inb.ga4gh.beacon.framework.model.v200;

import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestParameters;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonValue;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbException;
import jakarta.json.bind.serializer.DeserializationContext;
import jakarta.json.bind.serializer.JsonbDeserializer;
import jakarta.json.stream.JsonParser;
import java.lang.reflect.Type;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dmitry Repchevsky
 */

public class RequestParametersDeserializer implements JsonbDeserializer<BeaconRequestParameters> {

    private static final Jsonb jsonb = JsonbBuilder.create();
    @Override
    public BeaconRequestParameters deserialize(JsonParser parser, DeserializationContext dc, Type type) {
        final JsonValue value = parser.getValue();
        if (value != null && value.getValueType() == JsonValue.ValueType.OBJECT) {
            final JsonObject obj = value.asJsonObject();
            if (!obj.isEmpty()) {
                try {
                    return jsonb.fromJson(value.toString(), GenomicVariationsRequestParameters.class);
//                    return dc.deserialize(GenomicVariationsRequestParameters.class, 
//                            Json.createParserFactory(Collections.EMPTY_MAP).createParser(obj));
                } catch(JsonbException ex) {
                    Logger.getLogger(RequestParametersDeserializer.class.getName()).log(
                            Level.INFO, ex.getMessage());
                }
            }
        }
        return null;
    }
    
}
